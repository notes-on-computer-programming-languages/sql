# sql

Structured Query Language. https://en.m.wikipedia.org/wiki/SQL

# Books
* *Fundamentals of database systems*
  2016 (Seventh edition) Ramez Elmasri

# Documentation
* [*SQL*](https://en.m.wikipedia.org/wiki/SQL)
* [*SQL | DDL, DQL, DML, DCL and TCL Commands*
  ](https://www.geeksforgeeks.org/sql-ddl-dql-dml-dcl-tcl-commands/)

# Data Definition Language (DDL)
* [*Data definition language*
  ](https://en.m.wikipedia.org/wiki/Data_definition_language)

# Sample Databases
## SQLite
* [Introduction to chinook SQLite sample database
  ](https://www.sqlitetutorial.net/sqlite-sample-database/)

# Performance
* [*Enhancing Symfony Application Performance: Overcoming Many-to-Many Challenges with Postgres and ClickHouse*
  ](https://dev.to/alexrozz/enhancing-symfony-application-performance-overcoming-many-to-many-challenges-with-postgres-and-clickhouse-36g)
  2023-09 alexrozz (DEV)
